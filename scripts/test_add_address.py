import logging.handlers
import time
import pytest
import utils
from page.add_address_page05 import AddAddressPage
from page.address_list_page04 import AddressListPage
from page.index_page01 import IndexPage
from page.login_page03 import LoginPage
from page.my_page02 import MyPage
class TestAddAddress:
    def setup_class(self):
        # 进入地址管理列表页
        # 创建各个页面对象
        self.index_page = IndexPage()
        self.my_page = MyPage()
        self.login_page = LoginPage()
        self.address_list_page = AddressListPage()
        self.add_address_page = AddAddressPage()
        # 进入我的页面
        self.index_page.go_my_page()
        # 进入登录页面
        self.my_page.go_login_page()
        # 登录操作
        self.login_page.go_login("13400000001", "123456")
        time.sleep(3)
        # 进入地址管理列表页
        self.my_page.go_address_list_page()
    def teardown_class(self):
        # 关闭浏览器驱动
        utils.DriverUtil.quit_app_driver()
    def setup(self):
        # 通过切换activity进入地址管理列表页
        utils.switch_to_activity("com.tpshop.malls.activity.person.address.SPConsigneeAddressListActivity")
    def teardown(self):
        time.sleep(2)
    #读取json文件
    data=utils.build_data("add_address.json")
    #使用@pytest方法读取数据
    @pytest.mark.parametrize("desc, receiver, mobile, province, city, address, area, town, expect",data)
    def test_add_address(self, desc, receiver, mobile, province, city, address, area, town, expect):
        #生成测试报告内容
        logging.info(f"desc={desc}, receiver={receiver}, mobile={mobile},"
                     f"province={province}, city={city}, address={address}, "
                     f"area={area}, town={town}, expect={expect}")
        try:
            # 进入新增地址页
            self.address_list_page.go_add_address_page()
            # 新增地址
            self.add_address_page.add_new_address(receiver, mobile, province, city, address, area, town)
            actual_msg = utils.get_toast_msg(expect)
            print(actual_msg)
            assert expect in actual_msg
        except Exception as e:
            utils.get_screenshot("test_add_address")
            raise e