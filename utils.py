import json
import time
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
import config
# 封装跳转指定activity的方法
def switch_to_activity(activity_name, package_name='com.tpshop.malls'):
    DriverUtil.get_app_driver().start_activity(package_name, activity_name)
# 封装获取toast消息方法
def get_toast_msg(message):
    loc = (MobileBy.XPATH, f"//*[contains(@text,'{message}')]")
    try:
        el = WebDriverWait(DriverUtil.get_app_driver(), 10).until(
            lambda x: x.find_element(*loc)
        )
        return el.text
    except Exception as e:
        print(f"没有找到toast元素{loc}")
        raise e
# 封装数据处理函数可读取json文件
def build_data(filename):
    filepath = config.BASE_DIR + "/data/" + filename
    test_data = []
    with open(filepath, encoding='utf-8') as f:
        data = json.load(f)
        for case_data in data:
            test_data.append(tuple(case_data.values()))
    return test_data
# 封装获取屏幕截图方法
def get_screenshot(func_str="test"):
    filename = time.strftime("%Y%m%d-%H%M%S") + '.png'
    path = config.BASE_DIR + "/screenshot/" + func_str + filename
    DriverUtil.get_app_driver().get_screenshot_as_file(path)
#封装驱动工具
class DriverUtil:
    __driver = None
    @classmethod
    # 封装启动驱动
    def get_app_driver(cls):
        if cls.__driver is None:
            cls.__driver = webdriver.Remote(config.SERVER, config.DESIRED_CAPS)
        return cls.__driver
    @classmethod
    # 封装退出驱动
    def quit_app_driver(cls):
        if cls.__driver is not None:
            cls.get_app_driver().quit()
            cls.__driver = None