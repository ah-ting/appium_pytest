'''4. 地址管理列表页
元素：
1）新建地址
add_address_tv_loc = (MobileBy.ID, "com.tpshop.malls:id/add_address_tv")
业务方法：
go_add_address_page()
'''
from appium.webdriver.common.mobileby import MobileBy
from base.base_page import BasePage
class AddressListPage(BasePage):
    def __init__(self):
        super(AddressListPage, self).__init__()
        #新增地址元素
        self.add_address_tv_loc = (MobileBy.ID, "com.tpshop.malls:id/add_address_tv")
    #去新增地址元素
    def go_add_address_page(self):
        self.click_el(self.add_address_tv_loc)