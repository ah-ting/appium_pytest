'''2.我的页面
元素：
1）头像图片
profile_img_loc = (MobileBy.ID, "com.tpshop.malls:id/head_img")
2）地址管理
address_loc = (MobileBy.XPATH, "//*[@text='地址管理']")
业务方法：
1）去登录
go_login_page()
2）去地址管理
go_address_list_page()
'''
from appium.webdriver.common.mobileby import MobileBy
from base.base_page import BasePage
class MyPage(BasePage):
    def __init__(self):
        super().__init__()
        # 1）头像图片
        self.profile_img_loc = (MobileBy.ID, "com.tpshop.malls:id/head_img")
        # 2）地址管理
        self.address_loc = (MobileBy.XPATH, "//*[@text='地址管理']")
    # 进入登录页面
    def go_login_page(self):
        self.click_el(self.profile_img_loc)
    # 进入地址管理列表页面
    def go_address_list_page(self):
        # 滑动找到目标地址管理元素点击
        self.swipe_find_el(self.address_loc)
        self.click_el(self.address_loc)