'''3. 登录页面
元素：
1）账号
mobile_loc = (MobileBy.ID, "com.tpshop.malls:id/mobile_et")
2）密码
pwd_loc = (MobileBy.ID, "com.tpshop.malls:id/pwd_et")
3）同意选框
agree_btn_loc = (MobileBy.ID, "com.tpshop.malls:id/agree_btn")
4)登录按钮
login_tv_loc =(MobileBy.ID,"com.tpshop.malls:id/login_tv")
业务方法：
1）去登录
go_login()
'''
from appium.webdriver.common.mobileby import MobileBy
from base.base_page import BasePage
class LoginPage(BasePage):
    def __init__(self):
        super(LoginPage, self).__init__()
        #1）账号
        self.mobile_loc = (MobileBy.ID, "com.tpshop.malls:id/mobile_et")
        # 2）密码
        self.pwd_loc = (MobileBy.ID, "com.tpshop.malls:id/pwd_et")
        # 3）同意选框
        self.agree_btn_loc = (MobileBy.ID, "com.tpshop.malls:id/agree_btn")
        # 4)登录按钮
        self.login_tv_loc =(MobileBy.ID,"com.tpshop.malls:id/login_tv")
    def go_login(self, username, pwd):
        #输入数据点击同意选框和登录
        self.input_text(self.mobile_loc, username)
        self.input_text(self.pwd_loc, pwd)
        self.click_el(self.agree_btn_loc)
        self.click_el(self.login_tv_loc)