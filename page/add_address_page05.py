'''5. 新增地址页
元素：
1）收货人
receiver_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_name_et")
2）手机号码
mobile_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_mobile_et")
3）所在地区
region_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_region_tv")
3-1  省
province_loc = (MobileBy.XPATH, "//*[@text='{}']")
3-2  市
city_loc = (MobileBy.XPATH, "//*[@text='{}']")
3-3  区
area_loc = (MobileBy.XPATH, "//*[@text='{}']")
3-4  镇
town_loc = (MobileBy.XPATH, "//*[@text='{}']")
3-5 确定
Button_loc = (MobileBy.XPATH, "//*[@text='确定']")
4）详细地址
address_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_address_et")
5）设为默认地址
set_default_loc = (MobileBy.ID, "com.tpshop.malls:id/set_default_sth")
6）保存收货地址
save_address_loc = (MobileBy.ID, "com.tpshop.malls:id/save_tv")
业务方法：
add_new_address(receiver, mobile, province, city, address, area=None, town=None)'''
from appium.webdriver.common.mobileby import MobileBy
from base.base_page import BasePage
class AddAddressPage(BasePage):
    def __init__(self):
        super(AddAddressPage, self).__init__()
        # 1）收货人
        self.receiver_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_name_et")
        # 2）手机号码
        self.mobile_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_mobile_et")
        # 3）所在地区
        self.region_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_region_tv")
        # 3 - 1省
        self.province_loc = (MobileBy.XPATH, "//*[@text='{}']")
        # 3 - 2市
        self.city_loc = (MobileBy.XPATH, "//*[@text='{}']")
        # 3 - 3区
        self.area_loc = (MobileBy.XPATH, "//*[@text='{}']")
        # 3 - 4镇
        self.town_loc = (MobileBy.XPATH, "//*[@text='{}']")
        #3-5 确定
        self.Button_loc = (MobileBy.XPATH, "//*[@text='确定']")
        # 4）详细地址
        self.address_loc = (MobileBy.ID, "com.tpshop.malls:id/consignee_address_et")
        # 5）设为默认地址
        self.set_default_loc = (MobileBy.ID, "com.tpshop.malls:id/set_default_sth")
        # 6）保存收货地址
        self.save_address_loc = (MobileBy.ID, "com.tpshop.malls:id/save_tv")
    def add_new_address(self,receiver, mobile, province, city, address, area=None, town=None):
        # 1）输入收货人# 2）输入手机号码# 3）点击所在地区
        self.input_text(self.receiver_loc,receiver)
        self.input_text(self.mobile_loc,mobile)
        self.click_el(self.region_loc)
        #选择# 3 - 1省2市3区4镇5 点击确定
        new_province_loc=self.province_loc[0],self.province_loc[1].format(province)
        self.swipe_find_el(new_province_loc)
        self.click_el(new_province_loc)
        # 选择# 3 - 2市3区4镇5 点击确定
        new_city_loc=self.city_loc[0],self.city_loc[1].format(city)
        self.swipe_find_el(new_city_loc)
        self.click_el(new_city_loc)
        # 选择# 3 - 3区4镇5 点击确定
        if area is not None:
            new_area_loc = self.area_loc[0], self.area_loc[1].format(area)
            self.swipe_find_el(new_area_loc)
            self.click_el(new_area_loc)
        # 选择# 3 - 4镇5 点击确定
        if town is not None:
            new_town_loc = self.town_loc[0], self.town_loc[1].format(town)
            self.swipe_find_el(new_town_loc)
            self.click_el(new_town_loc)
        # 选择# 3 - 5 点击确定
        self.click_el(self.Button_loc)
        # 4）输入详细地址
        self.input_text(self.address_loc,address)
        # 5）点击设为默认地址
        self.click_el(self.set_default_loc)
        # 6）点击保存收货地址
        self.click_el(self.save_address_loc)