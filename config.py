import logging.handlers
import os#定义绝对路径
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SERVER = "http://127.0.0.1:4723/wd/hub"
DESIRED_CAPS = {#参数数据
    "platformName": "Android",# 系统型号
    "deviceName": "xxx",#设备号
    "appPackage": "com.tpshop.malls",#APP的包名
    #app打开的第一个界面
    "appActivity": "com.tpshop.malls.SPMainActivity",
    "autoGrantPermissions": True#是否自动授权
}
# 日志收集配置
def init_log_config():
    # 创建日志器对象
    logger = logging.getLogger()
    #定义日志类型INFO
    logger.setLevel(logging.INFO)
    # 创建处理器（控制台、文件）
    sh = logging.StreamHandler()
    #定义日志生成路径和文件名
    filename = BASE_DIR + "/log/tpshop.log"
    # 配置信息
    fh = logging.handlers.TimedRotatingFileHandler(filename, when='midnight',
                                                   interval=1, backupCount=15,
                                                   encoding="UTF-8")
    # 创建格式化器
    fmt = "%(asctime)s %(levelname)s [%(filename)s(%(funcName)s:%(lineno)d)] - %(message)s"
    formatter = logging.Formatter(fmt)
    # 为处理器设置格式
    sh.setFormatter(formatter)
    fh.setFormatter(formatter)
    # 把处理器添加到日志器中
    logger.addHandler(sh)
    logger.addHandler(fh)